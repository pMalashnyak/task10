package com.epam.Malashnyak.Anotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class CustomAnotation {
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface MyCustomAnnotation{
        int firstArg() default 15;
        String name();
        String info() default "";
    }
}
