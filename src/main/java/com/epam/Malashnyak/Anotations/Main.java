package com.epam.Malashnyak.Anotations;

import java.lang.Class;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
  public static void main(String[] args)
      throws InvocationTargetException, IllegalAccessException, NoSuchMethodException,
          NoSuchFieldException {
    Class cl = ClassWithFields.class;
    Field[] fields = cl.getDeclaredFields();
    for (Field fl : fields) {
      if (fl.getDeclaredAnnotations().length > 0) {
        System.out.println("Field: " + fl.toString());
        Annotation[] ans = fl.getDeclaredAnnotations();
        for (Annotation an : ans) {
          System.out.println("Annotation: " + an);
        }
      }
    }

    ClassWithFields clInst = new ClassWithFields();
    Method mt1 = cl.getDeclaredMethod("firstMethod");
    mt1.invoke(clInst);

    Method mt2 = cl.getDeclaredMethod("secondMethod");
    mt2.setAccessible(true);
    System.out.println((String) mt2.invoke(clInst));

    Method mt3 = cl.getDeclaredMethod("thirdMethod", int.class, int.class);
    mt3.setAccessible(true);
    System.out.println(mt3.invoke(clInst, 3, 5));

    Method mt4 = cl.getDeclaredMethod("MyMethod", String.class, int[].class);
    int[] ints = {1, 2, 3, 4, 5, 6, 7};
    mt4.invoke(clInst, "Ints= ", ints);

    Method m = cl.getDeclaredMethod("MyMethod", String[].class);
    Method mt5 = cl.getDeclaredMethod("MyMethod", String[].class);
    String[] strs = {"Frst", "Scnd", "Thrd", "Frth", "Ffth"};
    m.invoke(clInst, new Object[] {strs});

    Class tcl = TemplateClass.class;

    TemplateClass<?> templateClass = new TemplateClass<Object>();
    System.out.println(templateClass.frst + "\n");
    templateClass.setF();
    System.out.println(templateClass.frst + "\n");

    System.out.println(tcl.toString());
    for (Field fl : tcl.getDeclaredFields()) {
      System.out.println(fl.toString());
    }
    for (Method mt : tcl.getDeclaredMethods()) {
      System.out.println(mt);
    }
  }
}
